# L3 Analyse Discours Press

**Auteur: Etienne Toureille, Université de Rouen - UMR 6266 IDDES**


## Présentation

Ce projet contient les protocoles pour la création des données d'exercice du cours de Licence 3 de Géographie "Analyse de discours géographiques" de l'Université de Rouen Normandie.

Chaque étape peut ainsi être motifiée pour construire votre corpus personnel à partir de données Europresse. 

## Liste des programmes disponibles


Dans la racine du projet, plusieurs programmes au format .qmd (nouveau .rmd) sont disponibles:

- **import_europress_html.qmd**: ce programme permet d'extraire les données textuelles (texte et métadonnée) des pages html téléchargées depuis Europresse grâce à des fonctions de scraping.

- **tag_countries_newsmap**: ce programme permet de détecter des termes à partir d'un dictionnaire, d'analyser leurs fréquence et d'ajouter des colonnes identifiant la présence/absence de ces termes (sous la forme de booléens) au tableau créé avec le programme précédent. Dans cet exemple, on utilise un dictionnaire permettant de détecter les Etats du monde disponible dans le package *newsmap*.

- **convert_to_txm_format.qmd**: ce dernier programme permet de convertir un tableau de données issu de l'étape précédente dans un format compatible avec TXM (ensemble de fichiers .txt + un tableau au format .csv comportant les id et les métadonnées).


*__Note sur convert_to_txm_format.qmd__:  ce programme peut être repris pour convertir n'importe quel tableau au format TXM, et pas uniquement des données Europresse. Dans ce cas, un peu de travail sera nécessaire pour changer les colonnes, etc.*
