Le gouvernement veut « tripler l’effort » pour renouveler  les forêts

        L’un des objectifs du plan national présenté par  le ministre  de l’agriculture, Marc Fesneau, est  de replanter un milliard d’arbres d’ici à dix ans
